import sys
import os
import json
from pprint import pprint
import glob
from yaml import load, dump
try:
    from yaml import CLoader as Loader, CDumper as Dumper
except ImportError:
    from yaml import Loader, Dumper
import logging
logger = logging.getLogger(__name__)
 
def clean_parms(parms,slug):
    if parms.has_key(slug):
        return parms[slug]
    else:
        return parms 
    
def get_parms(slug=None):
    
    """
    Args:
        slug (Optional[str]): This arg is used on workstations where 
        multiple slugs or projects exists.  Ideally there is one VM per project
        but on occasion can host multiple projects on a single VM.  
        The ~/.bootops.yaml file must have a top level key being the slug arg 
        and the values will be settings for the slug.  On remote servers the use 
        of slug is not allowed since there is no concept of multiple slugs.
        
        If parms is used with slug=None and nested with multiple slugs then 
        first slug is used.  To change the default thenb change the order 
        in .bootops.yaml file
        
        When slug is none then then ~/.bootops.yaml will look like the below.
        
    .. code-block:: yaml

       environment: development
       nodename: local-local-local-1
       ipaddress: 10.0.2.15
       location: sf
       datacenter: do
       server_type: local
       slug: feed
       class_path: /home/ubuntu/workspace/bootops/classes
       settings_path: /home/ubuntu/workspace/my-settings/
        
    Returns:
        dictionary 
        
    Raises:
        AttributeError: ToDo: no ~/.bootops.yaml or bad form
        ValueError: ToDo: raise error if slug does not exists
        

    Examples:
        test1:
            datacenter: aws
        test2:
            datacenter: do
            
        >>> parms = get_parms(slug='test1')
        >>> print parms
        parms = {"datacenter": "aws"}
        
        datacenter: aws
        
        >>> parms = get_parms()
        >>> print parms
        parms = {"datacenter": "aws"}
        
    
    Tests:
        #Flat
        parms = get_parms(slug='feed1') False
        parms = get_parms(slug='feed') True
        parms = get_parms() True
        
    """
    HOME = os.path.expanduser("~")
    f = open('%s/.bootops.yaml' % HOME)
    meta_data = load(f, Loader=Loader)
    f.close()
    
    is_slug = False
    is_flat = False
    if meta_data.has_key('slug')==True:
        is_flat = True
        
    
    
    if is_flat==True:
        if slug:
            if meta_data['slug']==slug:
                parms = meta_data
                is_slug=True
        else:
            parms = meta_data
            is_slug=True
        
    if is_flat==False:  
        if meta_data.has_key(slug)==True and meta_data[slug]['slug']==slug:
            is_slug = True
            parms = meta_data[slug]
        
    
    
    
    if is_slug==False:
        print 'ERROR slug does not in the %s/.bootops.yaml file: %s' % (HOME,slug)
        exit()
    

    if parms.has_key('settings_path'):
        settings_path = '%s/*' % parms['settings_path']
    else:
        logger.error('Settings path does not exists in ~/bootops.yaml')
        exit()
        

    error_list = []
    for name in glob.glob(settings_path):
        
        if os.path.isdir(name):
            for meta in glob.glob('%s/*'%name):
                try:
                    with open(meta) as json_file:
                        json_data = json.load(json_file)
                    temp = {}
                    key = meta.split('/')[-1].split('.')[0]
                    temp[key] = json_data
                    parms = dict(parms.items() + temp.items())
                except:
                    error_list.append(meta)
    
    if error_list:
        print 'JSON Formatting with the below...'  
        for err in error_list:
            print err  
        exit()        
  
    
    return parms
 
# parms = get_parms(slug='zen')
# pprint(parms)






