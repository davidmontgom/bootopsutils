from setuptools import setup
#https://python-packaging.readthedocs.org/en/latest/dependencies.html
#https://gehrcke.de/2014/02/distributing-a-python-command-line-application/
#python setup.py develop
setup(name='bootopsutils',
      version='0.1.1',
      description='Bootopsutils for the cloud',
      url='http://github.com/davidmontgom/bootopsutils',
      author='Daland Montgomery',
      author_email='davidmontgomery@gmail.com',
      license='GPLv3',
      packages=['bootopsutils'],
      install_requires=[
          'pyyaml'
      ],
      zip_safe=False)