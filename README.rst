============
Installation
============

Download the bootopsutils repo.

.. code-block:: bash

   git clone git@bitbucket.org:davidmontgom/bootopsutils.git
   cd bootops
   sudo python setup.py install
   
When working in a another branch other then master then the below is the recomended install:

.. code-block:: bash

   git clone git@bitbucket.org:davidmontgom/bootopsutils.git
   cd bootopsutils
   git chechout {branch}
   sudo python setup.py develop
   
In your home directory create the below yaml file and edit the key/value pairs as needed.  All are required. For ip address use whatever on a workstation.  Also on a workstaion local-local-local-1 is required for the node_name. 

.. code-block:: yaml

   $ cat > ~/.bootops.yaml << EOF
     myslug:
     AWS_ACCESS_KEY_ID: xxxxxxxxxxxxxxxxxxx
     AWS_SECRET_ACCESS_KEY: yyyyyyyyyyyyyyyy
     environment: development
     nodename: local-local-local-1
     ipaddress: 10.0.2.15
     location: ny
     datacenter: do
     server_type: local
     slug: myslug
     class_path: /PATH/TO/bootops/classes
     settings_path: /PATH/TO/myslug-settings
    EOF

At the command line...

.. code-block:: python


   >>> import bootopsutils
   >>> bootopsutils.get_parms(slug='myslug')
   
    
